import com.google.cloud.pubsub.v1.MessageReceiver
import com.google.cloud.pubsub.v1.Publisher
import com.google.cloud.pubsub.v1.Subscriber
import com.google.protobuf.ByteString
import com.google.pubsub.v1.ProjectSubscriptionName
import com.google.pubsub.v1.ProjectTopicName
import com.google.pubsub.v1.PubsubMessage
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit


const val projectId = "tzero-nonprod"
const val topicId = "damo-master"
const val subscriptionId = "damo-master-sub"
const val consumingDuration = 30L

/**
 * The approach of this IT assumes all the Pub/Sub resources already exist.
 *
 * Generally dev (or service-accounts) don't have the required Pub/Sub Admin access to configure dead letter topics,
 * so request DevOps to spin one up for you, or use [projectId], [topicId], [subscriptionId] and "damo-dlq" if
 * they have not been wiped.
 *
 * Currently there is no real need to automate this and hard-coded resources should work fine.
 *
 * To measure retry frequency modify the following in GCP:
 * - max_delivery_attempts
 * - minimumBackoff
 * - maximumBackoff
 *
 * Also, modify the [consumingDuration] used. If there is no active subscription to read (and in our case nack)
 * the message, then the message stays un-delivered on the source topic.
 */
class DeadLetterQueueIT {

    @Test
    fun `fail, retry, measure time between retries`() {
        publish(projectId, topicId)
        receive(projectId, subscriptionId)
    }
}

fun publish(projectId: String, topicId: String) {
    val topicName = ProjectTopicName.of(projectId, topicId)
    println("Publish to: $topicName")
    val publisher = Publisher.newBuilder(topicName).build()
    val data = ByteString.copyFromUtf8("Hello from ${LocalDateTime.now().withNano(0)}")
    val pubSubMessage = PubsubMessage.newBuilder().setData(data).build()
    publisher.publish(pubSubMessage).get()
}

fun receive(
    projectId: String, subscriptionId: String
) {
    val subscriptionName = ProjectSubscriptionName.of(projectId, subscriptionId)

    // Instantiate an asynchronous message receiver.
    val receiver = MessageReceiver { message, consumer -> // nack the received message
        println("Id: " + message.messageId)
        println("Delivery Attempt: " + Subscriber.getDeliveryAttempt(message))
        println("Receive Time: " + LocalDateTime.now().withNano(0))
        consumer.nack()
    }

    val subscriber = Subscriber.newBuilder(subscriptionName, receiver).build()
    subscriber.startAsync().awaitRunning()
    System.out.printf("Listening for messages on %s:\n", subscriptionName.toString())
    /**
     * Allow the subscriber to run for 30s unless an unrecoverable error occurs.
     */
    subscriber.awaitTerminated(consumingDuration, TimeUnit.MINUTES)
    subscriber!!.stopAsync()

}